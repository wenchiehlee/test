[![](https://img.shields.io/badge/https-taes50232.gitlab.io/test-green)](https://taes50232.gitlab.io/test)

## Pre-Alpha Project

|Sites| Progress | Status | Live |
|-----| ------ | ------ | ------ |
|Mkdocs|![Prgress](https://img.shields.io/badge/progress-working-red)| [![pipeline status](https://gitlab.com/taes50232/test/badges/master/pipeline.svg)](https://gitlab.com/wjlee-myqnapcloud-com/wjlee-myqnapcloud-com/pipelines)| [![](https://img.shields.io/badge/WikiService-mkdocs-green)](https://wjlee-myqnapcloud-com.gitlab.io/wjlee-myqnapcloud-com)|

## 透過電腦取得小米藍芽溫溼度計資料

NEED

* 可以執行python語法的程式的工具
* 擷取資料的技術且定期擷取

即可取得「當下的」資料

## 整理資料並繪製圖表

NEED

* 有檔案紀錄當前資料
* 呈現所有資料


## References

* [material design palette](https://www.materialpalette.com/)
* [藍芽溫濕度計使用](https://ithelp.ithome.com.tw/articles/10224376?sc=rss.iron)

